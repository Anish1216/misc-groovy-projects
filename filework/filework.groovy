// File: filework.groovy
// Author: Anish K.
// Description: Basic file I/O with Groovy.

def fd = 'TEST.txt'
def file = new File(fd)

// If the specified path doesn't exist, create the file.
if (!file.exists()) {
	file.createNewFile()
	println 'Creating new file...\n'
}

file.text = 'Hello, world!\n'	// Will be overwritten

file.write 'My name is Anish.\n'	// !!!! THIS OVERWRITES THE FILE !!!!
file << 'I am from the USA.\n'
file.append 'I am programming in Groovy!\n'

def lines = file.readLines()

// Like Python's enumerate()
lines.eachWithIndex { line, index -> println "LINE $index: $line" }
println()

file.text = 'Hello, world!\n'	// Overwrites file
println file.readLines()
println()
file.delete()

file = new File('loremipsum.txt')
println 'Lorem ipsum text file - first letter of each word:'
lines = file.readLines()
def words = lines[0].split()
words.collect { word -> print word[0].toUpperCase() }