# README #
## Miscellaneous Groovy Projects ##
### Author: Anish Krishnan ###

This is where I will push some basic Groovy scripts/projects as I work on them. As of now (9/30/16), I have only been working with Groovy for a few days, so I am still learning.

### Projects ###
- closures.groovy - A basic rundown of Groovy closures
- personality-test.groovy - A personality test based on the MBTI that reads questions from a JSON file