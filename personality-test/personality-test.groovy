// File: personality-test.groovy
// Author: Anish K.
// Description: This program imports a JSON file with questions and answer codes.
// The program then takes user input and interprets it based on the answer codes provided. It provides the user with a final score based on all inputs.

import groovy.json.JsonSlurper

import java.nio.file.Path
import java.nio.file.Paths

class Question {
	static int ei = 0, ns = 0, ft = 0, jp = 0

	String text
	String[] choices
	def answers = [:]

	// Each letter is a code found in the JSON file and corresponds to a block of code found here.
	def answerMap = [
		'e': { ei ++ },
		'i': { ei -- },
		'n': { ns ++ },
		's': { ns -- },
		'f': { ft ++ },
		't': { ft -- },
		'j': { jp ++ },
		'p': { jp -- }
	]

	String response

	// Must be within the class (!?)
	def input() {
		System.console().readLine()
	}

	def printChoices() {
		choices.each { choice -> println choice }
	}

	def printAnswers() {
		answers.each { k, v -> println "$k ==> $v" }
	}

	static getScore() {
		return [ei, ns, ft, jp]
	}

	static getScoreAsString() {
		String type = ''
		type += (ei > 0) ? 'E' : 'I'
		type += (ns > 0) ? 'N' : 'S'
		type += (ft > 0) ? 'F' : 'T'
		type += (jp > 0) ? 'J' : 'P'

		return type
	}

	def calculate(String choice) {

		// For each answer code...
		answers[choice].each {
			c -> answerMap[c]()	// ...run the associated closure.
		}
	}

	def ask() {
		println('\n--------------------------------')
		println "$text\n"
		printChoices()
		println()

		// Defining possible answer options
		def options = []
		answers.each { entry -> options += entry.key }

		response = input().trim().toLowerCase()

		// Note: I tried (!response in options) but that failed.
		if (response in options == false) {
			println '\n---- Sorry, but that is not a valid response. ----'
			ask()
		} else {
			calculate(response)
		}
	}
}

def jsonSlurper = new JsonSlurper()
questionFile = Paths.get('questions.json').text

def questions = jsonSlurper.parseText(questionFile).questions
questionSet = []
questions.each {
	q -> questionSet += new Question(text: q.text, choices: q.choices, answers: q.actions.each { k, v -> [k: v] })
}

println()
println("Welcome to Anish's Personality Test.")
println("This questionnaire uses the Myers-Briggs Type Indicator (MBTI) to gauge your personality type.")
println()
println("Press [Enter] to begin.")
System.console().readLine()

for (question in questionSet) {
	question.ask()
}

String result = Question.getScoreAsString()

println('\n--------------------------------')
println('\n--------------------------------')

println "Your Myers-Briggs personality type: $result"
println "Visit www.celebritytypes.com/${result}.php for more information about your type."