// Name: closures.groovy
// Author: Anish K.
// Description: A simple overview of the capacities of closures in the Groovy programming language.

def counter = {
	int c = 0

	// Nested closure
	def increment = {
		c ++
	}
	
	increment
}

def add(int x, int y) {
	x + y
}

def addAlt = {
	int x, int y -> x + y
}

def addCurry = {
	int x ->
	{ int y -> x + y; }	// Nested closure
}

def getAndSet = {
	int init -> def gns = init
	println 'Initial value: $init'

	def get = { gns }	// Needs curly braces; returns gns when called

	def set = {
		int s -> gns = s
		println 'New value: $gns'
	}

	[get, set]
}

def myMap(xs, f) {
	ys = []
	for (x in xs) {
		ys += f(x)
	}
	ys
}

def myFilter(xs, f) {
	ys = []
	for (x in xs) {
		if (f(x)) ys += x
	}
	ys
}

// Emulating Data Transfer Objects (DTOs) with closures
def personFactory = {
	String nameIn, char genderIn, int ageIn -> def name = nameIn; def gender = genderIn; def age = ageIn

	def retrieve = {
		[
			'Name': name,
			'Gender': gender,
			'Age': age,
		]
	}

	retrieve
}

def employeeFactory = {
	String nameIn, String titleIn, int salaryIn -> def name = nameIn; def title = titleIn; def salary = salaryIn

	def retrieve = {
		[
			'Name': name,
			'Title': title,
			'Salary': salary,
		]
	}

	def setSalary = {
		newSalary -> salary = newSalary
	}

	[retrieve, setSalary]
}

class Calculator {

	def opMap = [
		'+': { n1, n2 -> n1 + n2 },
		'-': { n1, n2 -> n1 - n2 },
		'*': { n1, n2 -> n1 * n2 },
		'/': { n1, n2 -> n1 / n2 }
	]

	def calculate(int num1, String oper, int num2) {
		return opMap[oper](num1, num2)
	}
}


/*
 *	TESTING
 */

count = counter()
for (int i = 0; i < 10; i ++) {
	println 'Counter: ' + count()
}
println()

println '1 + 2 = ' + add(1, 2)
println '4 + 8 = ' + addAlt(4, 8)
println '16 + 32 = ' + addCurry(16)(32)
println()

def nums = [[1, 2], [3, 4], [5, 6], [7, 8]]
def sums = nums.collect { num1, num2 -> add(num1, num2) }
println sums
println()

def (get, set) = getAndSet(4)
println 'Get: ' + get()	// Prints 4
set(8)
println 'Get: ' + get()	// Prints 8
println()

list = [1, 2, 3, 4]
println '$list * 2 = ' + myMap(list){ int x -> 2 * x }
println '$list % 2 == 0: ' + myFilter(list){ it % 2 == 0 }	// Can use 'it' instead
println()

// Creates a simple immutable 'object'
def anish = personFactory('Anish', 'M' as char, 23)	// Because Groovy supports single-quote Strings
println 'Name: ' + anish()['Name']
println 'Gender: ' + anish()['Gender']
println 'Age: ' + anish()['Age']
println()

// Semi-mutable "object"
def (getObama, setPay) = employeeFactory('Barack Obama', 'President', 400000)
println getObama()
setPay(425000)	// The guy's got a tough job. Give him a raise!
println getObama()
println()

// Delegation: Only Calculator objects know how to calculate.
def add = { x, y -> calculate(x, '+', y)}
add.delegate = new Calculator()

println "4 + 8 = ${ add(4, 8) }"