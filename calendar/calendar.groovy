// File: calendar.groovy
// Author: Anish K.
// Description: A simple calendar program that allows the user to add, remove, and view events.

class Event {
	String name
	String description

	Date date

	def writeToFile(File output) {
		def dateString = date.format('yyyy-MM-dd')
		output << dateString + '\n'
		output << name + '\n'
		output << description + '\n'
		output << '----\n'
	}
}

def input() {
	System.console().readLine()
}

dayMap = [
	0: 'Sunday',
	1: 'Monday',
	2: 'Tuesday',
	3: 'Wednesday',
	4: 'Thursday',
	5: 'Friday',
	6: 'Saturday'
]

monthMap = [
	0: 'January',
	1: 'February',
	2: 'March',
	3: 'April',
	4: 'May',
	5: 'June',
	6: 'July',
	7: 'August',
	8: 'September',
	9: 'October',
	10: 'November',
	11: 'December',
]

File calendarFile = new File('calendar.txt')
if (!calendarFile.exists()) {
	calendarFile.createNewFile()
}

def addEvent(String dateString, String nameIn, String descriptionIn, File cFile) {
	def newDate = Date.parse('yyyy-MM-dd', dateString)

	def newEvent = new Event(date: newDate, name: nameIn, description: descriptionIn)
	newEvent.writeToFile(cFile)
}

def readToday(File cFile, Date date) {
	def read = false
	def lines = cFile.readLines()

	lines.each {
		line -> if (line == date.format('yyyy-MM-dd')) read = true; if (read) println line; if (line == '----') read = false
	}
}

def interpret(String line) {
	
}

currentDate = new Date()
choice = ''

println "Today's date is ${dayMap[currentDate.day]}, ${monthMap[currentDate.month]} ${currentDate.date}, ${currentDate.year + 1900}."

while (choice != 'quit') {
	println '\nWhat would you like to do?\n(1) See all events today\n(2) Add an event\n(3) Clear calendar\n'
	println 'Enter "quit" to exit the program.'
	choice = input().trim().toLowerCase()

	switch (choice) {
		case 'quit':
			System.exit(0);
		case '1':
			println()
			readToday(calendarFile, currentDate)
			break
		case '2':
			println 'Enter the date of the event (YYYY-MM-DD).'
			def eventDate = input()
			println 'Enter the name of the event.'
			def eventName = input()
			println 'Enter a brief description of the event.'
			def eventDesc = input()
			addEvent(eventDate, eventName, eventDesc, calendarFile)
			break
		case '3':
			println 'Are you sure you want to clear the calendar (Y/N)? This cannot be undone.'
			if (input().trim().toLowerCase()[0] == 'y') {
				calendarFile.write('')
			}
			break

		default:
			println 'Sorry, but that is not a valid option.'
			break
	}
}	// No do-while loop in Groovy :(